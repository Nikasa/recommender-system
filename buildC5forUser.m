function  out = buildC5forUser(Users, Rating, MoviesDesc)
    for i=1:length(Users)
        currentUser = Users(i);
        TrainRatingC5 = buildRatingC5(currentUser, Rating.train, MoviesDesc);
        VldRatingC5 = buildRatingC5(currentUser, Rating.validation, MoviesDesc);
        csvwrite(['user', num2str(currentUser),'.data'], TrainRatingC5);
        csvwrite(['user', num2str(currentUser),'.test'], VldRatingC5);    
        copyfile('Movielens.names', ['user', num2str(currentUser),'.names']);
        copyfile('Movielens.costs', ['user', num2str(currentUser),'.costs']);
    end
    out = 1;
end
function RatingC5 = buildRatingC5(userId,Rating, MoviesDesc)
    rateIdx = find(Rating(:, 1) == userId);
    rates = Rating(rateIdx, 3);
    moviesGenres = MoviesDesc(Rating(rateIdx, 2), 2:end);
    RatingC5 = [rates, moviesGenres];
    unknownTypes = find(RatingC5(:, 2)==1);
    RatingC5 = removerows(RatingC5, 'ind', unknownTypes);%remove unknown rates
    RatingC5(:, 2) = [];    
end