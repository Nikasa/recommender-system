function RMSE = buildSVDGD_New(Rating, RatingMatrix)
    %Should use nFeatures = 30
    nFeatures = 72;
    alpha = 0.005;
    lambda = 0.05;
    %momentum = 0.9;
    sd = 0.1;
    %nBatchs = 800;
    nInterations = 1000;
    nUsers = size(RatingMatrix.train, 1);
    nMovies = size(RatingMatrix.train, 2);
    
    %Rating = permute(reshape(Rating', 3, [], nBatchs), [3, 2, 1]);
    %nTrainPerBatch = size(Rating, 2);
    nRatings = size(Rating, 1);
    GlobalAvgRating = nansum(nansum(RatingMatrix.train))/sum(~isnan(RatingMatrix.train(:)))
    K = 25;
    AvgMovieRatings = (GlobalAvgRating*K+nansum(RatingMatrix.train, 1))./(K + nansum(~isnan(RatingMatrix.train), 1));
    AvgUserRatings = (GlobalAvgRating*K+nansum(RatingMatrix.train, 2))./( K + nansum(~isnan(RatingMatrix.train), 2));
    Pred = repmat(AvgMovieRatings, nUsers, 1)+repmat(AvgUserRatings, 1, nMovies)-GlobalAvgRating;
    Pred = zeros(nUsers, nMovies);
    RMSE = 2*ones(1, nFeatures);
    for x = 1:nFeatures
        U = sd*ones(nUsers, 1);
        V = sd*ones(nMovies,1);
        oldSumErr = Inf;
        for j = 1:nInterations
            sumErr = 0;
            for i = 1:nRatings
                userId = Rating(i, 1);
                movieId = Rating(i, 2);
                rating = Rating(i, 3);
                err = Pred(userId, movieId)+U(userId)*V(movieId) - rating;
                sumErr = sumErr + err^2+lambda*(U(userId)^2+V(movieId)^2);
                gradU = err*V(movieId)+lambda*U(userId);
                gradV = err*U(userId)+lambda*V(movieId);
                U(userId) = U(userId)-alpha*gradU;
                V(movieId) = V(movieId)-alpha*gradV;
            end
            if (sumErr>oldSumErr)
                break;
            else
                oldSumErr = sumErr;
            end
        end
        Pred = Pred + U*V';
        disp('Features');
        disp(x);
        [MAE Result] = calErr(Pred, RatingMatrix.validation);
        Result
    end
end