function RMSE = buildAVGPrediction(RatingMatrix)
    nUsers = size(RatingMatrix.train, 1);
    nItems = size(RatingMatrix.train, 2);
    AvgItemRating = nanmean(RatingMatrix.train, 1);
    AvgItemRating(isnan(AvgItemRating)) = 2.5;
    AvgItemRating = repmat(AvgItemRating, nUsers, 1);
    RatingMatrix.train(isnan(RatingMatrix.train)) = AvgItemRating(isnan(RatingMatrix.train));
    [MAE, RMSE] = calErr(RatingMatrix.train, RatingMatrix.validation);
end