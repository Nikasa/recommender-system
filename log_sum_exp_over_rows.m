function ret = log_sum_exp_over_rows(matrix) %Matrix: <numcases, numdims, numclasses>
  % This computes log(sum(exp(a), 1)) in a numerically stable way
  maxs_small = max(matrix, [], 3);
  maxs_big = repmat(maxs_small, [1, 1, size(matrix, 3)]);
  ret = log(sum(exp(matrix - maxs_big), 3)) + maxs_small;
  ret = repmat(ret, [1 1 size(matrix, 3)]);
end