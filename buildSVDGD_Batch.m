function RMSE = buildSVDGD_Batch(RatingMatrix, nIterations)
    nFeatures = 40;
    alpha = 0.0002;
    momentum = 0.9;
    lambda = 0.07;
    sd = 0.01;
    RMSE = zeros(1, nIterations);
    nUsers = size(RatingMatrix.train, 1);
    nMovies = size(RatingMatrix.train, 2);
    noRatingIdx = isnan(RatingMatrix.train);
    U = sd*randn(nUsers, nFeatures);
    V = sd*randn(nMovies,nFeatures);
    gradU = zeros(nUsers, nFeatures);
    gradV = zeros(nMovies, nFeatures);
    
    Pred = U*V';
    for i = 1:nIterations
        Pred(noRatingIdx) = 0;
        err = Pred-RatingMatrix.train;
        gradU = alpha*(err*V+lambda*U)+momentum*gradU;
        gradV = alpha*(err'*U+lambda*V)+momentum*gradV;
        U = U-gradU;
        V = V-gradV;
        Pred = U*V';
        
        [MAE RMSE(i)] = calErr(Pred, RatingMatrix.validation);
        RMSE(i)
    end
end