% Calculate similarity matrix
% Currently only using Pearson Correlation
%
% Users:        array of userId we want to calculate similarity with others.
% RatingMatrix: Original rating matrix [nUsersxnItems]
function Similarity = buildSimilarity(Users, RatingMatrix)
    nUsers = size(RatingMatrix, 1);
    nMovies = size(RatingMatrix, 2);
    
    AvgRating = nanmean(RatingMatrix, 2);
    %AvgRating = [4; 2.25; 3.5; 3; 3.25];
    AdjustedRatingMatrix = RatingMatrix - repmat(AvgRating, 1, nMovies);
    Similarity = zeros(nUsers, nUsers);
    
    for i = 1:length(Users)
        currentUser = Users(i);
            for j = 1:nUsers
                if (currentUser~=j)
                    Similarity(currentUser, j) = calPearsonCorr(AdjustedRatingMatrix(currentUser, :),...
                        AdjustedRatingMatrix(j, :));
                end
            end
    end
    
    %SimilarityMatrix = SimilarityMatrix + permute(SimilarityMatrix, [2, 1]);
    %SqrtErr = sqrt(sum((AdjustedRatingMatrix).^2, 2))
    %SqrtErrMatrix = SqrtErr*SqrtErr';

    %SimilarityMatrix = (AdjustedRatingMatrix * AdjustedRatingMatrix');
    %SimilarityMatrix = SimilarityMatrix./SqrtErrMatrix;
    %save('Similarity.mat', 'Similarity');
end
function corr = calPearsonCorr(u, v)
    cov = nansum(u.*v);
    if (cov == 0) 
        corr = 0;
    else
        intersectIdx = (~isnan(u))&(~isnan(v));
        corr = cov/sqrt(sum(u(intersectIdx).^2)*sum(v(intersectIdx).^2));
    end
end
