function RMSE = buildSVDGDPrediction(Rating, RatingMatrix, nIterations)
    nFeatures = 40;
    alpha = 5;
    lambda = 0.07;
    momentum = 0.9;
    sd = 0.01;
    nBatchs = 20;
    %nInterations = 70;
    nUsers = size(RatingMatrix.train, 1);
    nMovies = size(RatingMatrix.train, 2);
    U = sd*randn(nUsers, nFeatures);
    V = sd*randn(nMovies,nFeatures);
    
    Rating = permute(reshape(Rating', 3, [], nBatchs), [3, 2, 1]);
    nTrainPerBatch = size(Rating, 2);
    deltaU = zeros(nUsers, nFeatures);
    deltaV = zeros(nMovies, nFeatures);
    Pred = U*V';
    RMSE = zeros(1, nIterations);
    for x = 1:nIterations
        disp('Loop'); disp(x);
        for t = 1:nBatchs
            gradU = zeros(nUsers, nFeatures);
            gradV = zeros(nMovies, nFeatures);
            for i = 1:nTrainPerBatch
                userId = Rating(t, i, 1);
                movieId = Rating(t, i, 2);
                rating = Rating(t, i, 3);
                err = Pred(userId, movieId) - rating;
                gradU(userId, :) = gradU(userId, :) + err*V(movieId, :) + lambda*U(userId, :);
                gradV(movieId, :) = gradV(movieId, :) + err*U(userId, :) + lambda*V(movieId, :);
            end
            deltaU =-alpha*gradU/nTrainPerBatch+momentum*deltaU;
            deltaV =-alpha*gradV/nTrainPerBatch+momentum*deltaV;
            U = U + deltaU;
            V = V + deltaV;
            Pred = U*V';
        end
        Pred = U*V';
        [MAE RMSE(x)] = calErr(Pred, RatingMatrix.validation);
        RMSE(x)
    end
end