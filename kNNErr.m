function [Err, MAE, MSE] = kNNErr(RatingMatrix, Users, k)
    Similarity= buildSimilarity(Users, RatingMatrix.train);
    Prediction = buildKNNPrediction(Users, Similarity, RatingMatrix.train, k);
    Prediction = round(Prediction);
    Target = RatingMatrix.validation(Users, :);
    [Err, MAE, MSE] = calErr(Users, Prediction, Target);
end