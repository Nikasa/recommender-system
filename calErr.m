function [MAE, RMSE] = calErr(Prediction, Target)
    %Prediction = round(Prediction);
    Prediction(Prediction<=1) = 1;
    Prediction(Prediction>=5) = 5;
    if ~all(size(Prediction) == size(Target))
        disp('Err: Prediction and Target matrix dont match');
    else
        idx = ~isnan(Target(:));
        total = sum(idx);
        SSE = sum((Prediction(idx)-Target(idx)).^2);
        AE = sum(abs(Prediction(idx)-Target(idx)));
        RMSE = sqrt(SSE/total);
        MAE = AE/total;
    end
end