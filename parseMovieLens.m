% Parse Movielens dataset into matrix
%
% Input:
%      trainPercent: how many percent of data sets we want to use as training data.
%      vldPercent:   percent of validation data
% Output:
%      Rating.train, Rating.validation, Rating.test: Rating matrices[nUsersxnItems]
%      MoviesDesc:   Array of genres description of movies [nItemsxnGenres]
%      nRatesPerUser: Sorted array of userId by number of their ratings.
function [Rating MoviesDesc nRatesPerUser] = parseMovieLens(trainPercent, vldPercent, isTimeStampSort, isRetainRatio)
    fileDataId = fopen('Movielens.data');
    Rating = fscanf(fileDataId, '%d %d %d %d', [4, Inf]);
    Rating = Rating';
    if (isTimeStampSort)
        Rating = sortrows(Rating, 4);
    else
        Rating = Rating(randperm(size(Rating, 1)),:);
    end
    Rating(:, 4) = [];
    fclose(fileDataId);
    
    nUsers = max(Rating(:, 1));
    nMovies = max(Rating(:, 2));
    nRatesPerUser = zeros(nUsers, 2);
    nRatesPerUser(:, 1) = 1:nUsers;
    for i=1:nUsers
        nRates = length(find(Rating(:, 1) == i));
        nRatesPerUser(i, 2) = nRates;
    end
    nRatesPerUser = sortrows(nRatesPerUser, -2);
    
    MoviesDesc = textread('Movielens.item', '%s', 'delimiter','\n','whitespace','');
    MoviesId = arrayfun(@(x) regexp(x, '^\d*', 'match', 'once'), MoviesDesc);
    MoviesId = cellfun(@str2num,MoviesId);
    MoviesGenre = arrayfun(@(x) strrep(regexp(x, '(\|(0|1)){19}', 'match', 'once'), '|', ' '), MoviesDesc);
    MoviesGenre = cellfun(@(x) sscanf(x, ' %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d')', MoviesGenre, 'UniformOutput', false);
    MoviesGenre = cell2mat(MoviesGenre);
    
    MoviesDesc = [MoviesId MoviesGenre];
    tmp.train = []; tmp.validation = []; tmp.test = [];
    [tmp.train, tmp.validation, tmp.test] = splitData(Rating, isRetainRatio, trainPercent, vldPercent);
    Rating = tmp;
    RatingMatrix.train = buildRatingMatrix(Rating.train, nUsers, nMovies);
    RatingMatrix.validation = buildRatingMatrix(Rating.validation, nUsers, nMovies);
    RatingMatrix.test = buildRatingMatrix(Rating.test, nUsers, nMovies);

    save('MovieLens.mat', 'Rating', 'RatingMatrix', 'MoviesDesc', 'nRatesPerUser');
    
end
function [TrainData, VldData, TestData] = splitData(Rating, isRetainRatio, trainPercent, vldPercent)
    TrainData = []; VldData = []; TestData = [];
    nUsers = max(Rating(:, 1)); 
    if (isRetainRatio)
        for i = 1:nUsers
            RatingIdx = find(Rating(:, 1)==i);
            nRates = length(RatingIdx);
            vldSplitPoint = round(nRates*trainPercent);
            testSplitPoint = round(nRates*(trainPercent+vldPercent));
            TrainData = [TrainData; Rating(RatingIdx(1:vldSplitPoint), :)];
            VldData = [VldData; Rating(RatingIdx(vldSplitPoint+1:testSplitPoint), :)];
            TestData = [TestData; Rating(RatingIdx(testSplitPoint+1:end), :)];
        end
    else
        nRates = length(Rating);
        vldSplitPoint = round(nRates*trainPercent);
        testSplitPoint = round(nRates*(trainPercent+vldPercent));
        TrainData = Rating(1:vldSplitPoint, :);
        VldData = Rating(vldSplitPoint+1:testSplitPoint, :);
        TestData = Rating(testSplitPoint+1:end, :);
    end
end
function RatingMatrix = buildRatingMatrix(Rating, nUsers, nMovies)
    RatingMatrix = zeros(nUsers, nMovies);
    indx = Rating(:, 1) + (Rating(:, 2)-1).*nUsers;
    RatingMatrix(indx) = Rating(:, 3);
    RatingMatrix(RatingMatrix==0) = NaN;
end