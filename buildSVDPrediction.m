function RMSE = buildSVDPrediction(RatingMatrix, k)
    nItems = size(RatingMatrix.train, 2);
    %AvgRating = nanmean(RatingMatrix(:));
    %AvgUserRating = nanmean(RatingMatrix, 2);
    %AvgUserRating = repmat(AvgUserRating, 1, nItems);
    [RatingMatrix.train, AvgUserRating] = fillMissingRating(RatingMatrix.train);
    %prediction = AvgUserRating(Users, :) + RatingMatrix(Users, :);
    [U, S, V] = svd(RatingMatrix.train);
    UserLatentFactors = U(:, 1:k)*sqrt(S(1:k, 1:k));
    ItemLatentFactors = sqrt(S(1:k, 1:k))*V(:, 1:k)';
    
    Prediction = AvgUserRating + UserLatentFactors*ItemLatentFactors;
    [MAE RMSE] = calErr(Prediction, RatingMatrix.validation)
end
function [RatingMatrix AvgUserRating] = fillMissingRating(RatingMatrix)
    nUsers = size(RatingMatrix, 1);
    nItems = size(RatingMatrix, 2);
    AvgItemRating = nanmean(RatingMatrix, 1);
    AvgItemRating(isnan(AvgItemRating)) = 2.5;
    AvgItemRating = repmat(AvgItemRating, nUsers, 1);
  
    RatingMatrix(isnan(RatingMatrix)) = AvgItemRating(isnan(RatingMatrix));
    AvgUserRating = nanmean(RatingMatrix, 2);
    AvgUserRating = repmat(AvgUserRating, 1, nItems);
    RatingMatrix = RatingMatrix - nanmean(RatingMatrix(:));
end