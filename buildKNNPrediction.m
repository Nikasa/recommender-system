% Make prediction using kNN algorithm
%
% Users:        array of userID that we need to make prediction
% Similarity:   Similarity matrix [nUsersxnUsers], can calculate by Pearson
%               Correlation, Adjusted Cosine, or other metrics.
% RatingMatrix: Original rating matrix [nUsersxnItems], unknown ratings are marked by NaN
% k           : Number of neighbors we use to predict ratings.

function Prediction = buildKNNPrediction(Users, Similarity, RatingMatrix, k)
    nUsers = size(Similarity, 1);
    nItems = size(RatingMatrix, 2);
    AvgUserRating = nanmean(RatingMatrix, 2);
    AdjustedRatingMatrix = RatingMatrix - repmat(AvgUserRating, 1, nItems);
    Prediction = zeros(length(Users), nItems);
    
    [sortedSim, sortIdx] = sort(Similarity, 2, 'descend');
    for i = 1:length(Users)
        currentUser = Users(i);
        neighborsIdx = sortIdx(currentUser, :);        
        for currentItem = 1:nItems
            if isnan(RatingMatrix(currentUser, currentItem))

                hasRated = ~isnan(AdjustedRatingMatrix(neighborsIdx, currentItem)');
                positiveSim = (Similarity(currentUser, neighborsIdx)>0);
                GoodNeighbors = neighborsIdx.*hasRated.*positiveSim;
                GoodNeighbors = GoodNeighbors(GoodNeighbors~=0);
                if (length(GoodNeighbors)<k)
                    if (length(GoodNeighbors) == 0)
                        disp('NAN')
                        currentItem = currentItem
                        avg = AvgUserRating(currentUser)
                        Prediction(i, currentItem) = AvgUserRating(currentUser);
                        
                        continue;
                    end
                    kNeighborsIdx = GoodNeighbors;
                else
                    kNeighborsIdx = GoodNeighbors (1:k);
                end
                Prediction(i, currentItem) = AvgUserRating(currentUser)...
                    + sum(Similarity(currentUser, kNeighborsIdx).*AdjustedRatingMatrix(kNeighborsIdx,currentItem)')...
                    /sum(Similarity(currentUser, kNeighborsIdx));
                if (Prediction(i, currentItem)<1)
                    Prediction(i, currentItem) = 1;
                    %disp('ERR');
                    %currentUser
                    %currentItem
                    %sim = Similarity(currentUser, kNeighborsIdx)
                    %rate = AdjustedRatingMatrix(kNeighborsIdx,currentItem)'
                else
                    if (Prediction(i, currentItem)>5)
                        Prediction(i, currentItem) = 5;
                    end
                end
            end
        end
    end
end