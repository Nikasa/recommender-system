% Version 1.000 
%
% Code provided by Geoff Hinton and Ruslan Salakhutdinov 
%
% Permission is granted for anyone to copy, use, modify, or distribute this
% program and accompanying programs and documents for any purpose, provided
% this copyright notice is retained and prominently displayed, along with
% a note saying that the original programs are available from our
% web page.
% The programs and documents are distributed without any warranty, express or
% implied.  As the programs were written for research purposes only, they have
% not been tested to the degree that would be advisable in any important
% application.  All use of these programs is entirely at the user's own risk.

% This program trains Restricted Boltzmann Machine in which
% visible, binary, stochastic pixels are connected to
% hidden, binary, stochastic feature detectors using symmetrically
% weighted connections. Learning is done with 1-step Contrastive Divergence.   
% The program assumes that the following variables are set externally:
% maxepoch  -- maximum number of epochs
% numhid    -- number of hidden units 
% batchdata -- the data that is divided into batches (numcases numdims numbatches)
% restart   -- set to 1 if learning starts from beginning 
function RME = rbm(RatingMatrix, nEpochs)
    K = 5;
    %nEpochs = 100;
    numhid = 30;
    [nUsers, nMovies] = size(RatingMatrix.train);
    numbatches = 1;
    batchdata = zeros(nUsers, nMovies, K);
    RME = zeros(1, nEpochs);
    for i = 1:K
        temp = +(RatingMatrix.train == i);
        temp(isnan(RatingMatrix.train)) = NaN;
        batchdata(:, :, i) = temp;
    end
    restart = 1;

    epsilonw      = 0.2;   % Learning rate for weights 
    epsilonvb     = 0.2;   % Learning rate for biases of visible units 
    epsilonhb     = 0.2;   % Learning rate for biases of hidden units 
    weightcost  = 0.002;   
    initialmomentum  = 0.5;
    finalmomentum    = 0.9;

    
    
    if restart ==1,
      restart=0;
      epoch=1;

    % Initializing symmetric weights and biases. 
      vishid     = 0.1*randn(nMovies, numhid, K);%Weight
      hidbiases  = zeros(1,numhid);              %Hidden bias
      visbiases  = zeros(nMovies, K);             %Visible bias

      SumRates = sum(~isnan(RatingMatrix.train));
      SumRates(SumRates == 0) = 1;
      for i = 1:K
          visbiases(:, i) = log(sum(RatingMatrix.train == i)./SumRates);
      end
      visbiases(visbiases == -Inf) = -10;
      poshidprobs = zeros(nUsers,numhid); %positive hidden probability
      neghidprobs = zeros(nUsers,numhid); %negative hidden probability
      posprods    = zeros(nMovies,numhid, K);  %positive product
      negprods    = zeros(nMovies,numhid, K);  %negative product
      vishidinc  =  zeros(nMovies,numhid, K);   %weigh gradient
      hidbiasinc = zeros(1,numhid);         %hidden bias gradient
      visbiasinc = zeros(nMovies, K);        %visible bias gradient
      %batchposhidprobs=zeros(numcases,numhid,numbatches);
    end

    for epoch = epoch:nEpochs
     fprintf(1,'epoch %d\r',epoch); 
     errsum=0;
     for batch = 1:numbatches,
     fprintf(1,'epoch %d batch %d\r',epoch,batch); 

    %%%%%%%%% START POSITIVE PHASE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      data = batchdata(:,:,:,batch);                                           %<K, numcases, numdims>
      missingValue = isnan(data);
      data(missingValue) = 0;                                                  %Change missing value to 0
      poshidprobs = vis2hid(data);
      %batchposhidprobs(:,:,batch)=poshidprobs;                                 %
      posprods = zeros(nMovies, numhid, K);
      for i = 1:K
        posprods(:, :, i)= data(:, :, i)' * poshidprobs;                        %Compute <vi.hj>data
      end
      poshidact   = sum(poshidprobs);                                          %Compute Sum(positive hidden activation)
      posvisact = squeeze(sum(data));                                                %Compute Sum(positive visible activation)
      [MAE, RME(epoch)] = errValidation(poshidprobs)
    %%%%%%%%% END OF POSITIVE PHASE  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      %poshidstates = poshidprobs > rand(nUsers,numhid);                      %Change hidden probability into binary state

    %%%%%%%%% START NEGATIVE PHASE  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    neghidprobs = poshidprobs;
    for i = 1:3
      negdata = hid2vis(neghidprobs);
      negdata(missingValue) = 0;                                                %Change missing value to 0
      neghidprobs = vis2hid(negdata);
    end
    negprods = zeros(nMovies, numhid, K);
      for i = 1:K
          negprods(:,:,i)  = negdata(:, :, i)'*neghidprobs;                                         %Compute <vi.hj> model
      end

      neghidact = sum(neghidprobs);                                             %Compute Sum(negative hidden activation)
      negvisact = squeeze(sum(negdata));                                                 %Compute Sum(negative visible activation)


    %%%%%%%%% END OF NEGATIVE PHASE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      err= sum(sum(sum( (data-negdata).^2 )));                                       %Err of reconstruct visible from real visible input
      errsum = err + errsum;

       if epoch>5,
         momentum=finalmomentum;
       else
         momentum=initialmomentum;
       end;

    %%%%%%%%% UPDATE WEIGHTS AND BIASES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        vishidinc = momentum*vishidinc + ...
                    epsilonw*( (posprods-negprods)/nUsers - weightcost*vishid);       
        visbiasinc = momentum*visbiasinc + (epsilonvb/nUsers)*(posvisact-negvisact);
        hidbiasinc = momentum*hidbiasinc + (epsilonhb/nUsers)*(poshidact-neghidact);

        vishid = vishid + vishidinc;
        visbiases = visbiases + visbiasinc;
        hidbiases = hidbiases + hidbiasinc;

    %%%%%%%%%%%%%%%% END OF UPDATES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      end
      fprintf(1, 'epoch %4i error %6.1f  \n', epoch, errsum); 
    end
    function [MAE RME] = errValidation(poshidprobs)
        pred_classinput = zeros(nUsers, nMovies, K);
        for j = 1:K
            pred_classinput(:, :, j) = poshidprobs*vishid(:,:, j)'+repmat(visbiases(:, j)', nUsers, 1);
        end
        class_normalizer = log_sum_exp_over_rows(pred_classinput);
        log_class_prob = pred_classinput - class_normalizer;
        pred_prob = exp(log_class_prob);

        prediction = zeros(nUsers, nMovies);
        for j = 1:K
            prediction = prediction+pred_prob(:, :, j)*j;
        end
        [MAE, RME] = calErr(prediction, RatingMatrix.validation);
    end
    function hidprobs = vis2hid(data)
          temp = zeros(nUsers, numhid);
          for j = 1:K %Calculate sum(data*vishid)
              temp = temp + data(:, :, j)*vishid(:, :, j);
          end
          hidprobs = 1./(1 + exp(-temp - repmat(hidbiases,nUsers,1)));         %From input to hidden probability   
    end
    function negdata = hid2vis(hidprobs)
        hidstates = hidprobs > rand(nUsers, numhid);
        negdata_classinput = zeros(nUsers, nMovies, K);
        for j = 1:K
            negdata_classinput(:, :, j) = hidstates*vishid(:,:, j)'+repmat(visbiases(:, j)', nUsers, 1);
        end
        class_normalizer = log_sum_exp_over_rows(negdata_classinput);
        log_class_prob = negdata_classinput - class_normalizer;
        negdata = exp(log_class_prob);
    end
end
